defmodule Todo.List.Server do
  use GenServer

  @impl GenServer
  def init(state) do
    {:ok, state}
  end

  @impl GenServer
  def handle_call({:add_entry, entry}, _caller, state) do
    state = Todo.List.add_entry(state, entry)
    {
      :reply,
      :ok,
      state,
    }
  end

  @impl GenServer
  def handle_call({:update_entry, entry_id, update_fn}, _caller, state) do
    state = Todo.List.update_entry(state, entry_id, update_fn)
    {
      :reply,
      :ok,
      state,
    }
  end

  @impl GenServer
  def handle_call({:delete_entry, entry_id}, _caller, state) do
    state = Todo.List.delete_entry(state, entry_id)
    {
      :reply,
      :ok,
      state,
    }
  end


  @impl GenServer
  def handle_call({:entries, date}, _caller, state) do
    {
      :reply,
      Todo.List.entries(state, date),
      state,
    }
  end

  def start() do
    GenServer.start(__MODULE__, Todo.List.new())
  end

  def stop(pid) do
    GenServer.stop(pid, :shutdown)
  end

  def add_entry(pid, entry) do
    GenServer.call(pid, {:add_entry, entry})
  end

  def update_entry(pid, entry_id, update_fn) do
    GenServer.call(pid, {:update_entry, entry_id, update_fn})
  end

  def delete_entry(pid, entry_id) do
    GenServer.call(pid, {:delete_entry, entry_id})
  end

  def entries(pid, date) do
    GenServer.call(pid, {:entries, date})
  end



end
