defmodule Todo.App do
  @moduledoc """
  Documentation for `Todo.App`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Todo.App.hello()
      :world

  """
  def hello do
    :world
  end
end
