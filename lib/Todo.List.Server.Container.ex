defmodule Todo.List.Server.Container do
  use GenServer

  @impl GenServer
  def init(_) do
    {:ok, %{}}
  end


  def start() do
    # lazy autostart singleton??
    GenServer.start(__MODULE__, nil, name: __MODULE__)
  end

  def stop() do
    # FIXME stop all todo servers
    GenServer.stop(__MODULE__, :shutdown)
  end

  def _get_todo_list_pid(state, todo_list_id) do
    todo_list_pid = Map.get(state, todo_list_id)
    # FIXME more beautiful?
    if todo_list_pid == nil do
      {:ok, todo_list_pid} = Todo.List.Server.start()
      {
        Map.put(state, todo_list_id, todo_list_pid),
        todo_list_pid
      }
    else
      {
        Map.put(state, todo_list_id, todo_list_pid),
        todo_list_pid
      }
    end
  end


  def add_entry(todo_list_id, entry) do
    GenServer.call(__MODULE__, {:add_entry, todo_list_id, entry})
  end

  def entries(todo_list_id, date) do
    GenServer.call(__MODULE__, {:entries, todo_list_id, date})
  end

  def update_entry(todo_list_id, entry_id, update_fn) do
    GenServer.call(__MODULE__, {:update_entry, todo_list_id, entry_id, update_fn})
  end

  def delete_entry(todo_list_id, entry_id) do
    GenServer.call(__MODULE__, {:delete_entry, todo_list_id, entry_id})
  end

  @impl GenServer
  def handle_call({:add_entry, todo_list_id, entry}, _caller, state) do
    {state, todo_list_pid} = _get_todo_list_pid(state, todo_list_id)
    {
      :reply,
      Todo.List.Server.add_entry(todo_list_pid, entry),
      state
    }
  end

  @impl GenServer
  def handle_call({:entries, todo_list_id, date}, _caller, state) do
    {state, todo_list_pid} = _get_todo_list_pid(state, todo_list_id)
    {
      :reply,
      Todo.List.Server.entries(todo_list_pid, date),
      state
    }
  end

  @impl GenServer
  def handle_call({:update_entry, todo_list_id, entry_id, update_fn}, _caller, state) do
    {state, todo_list_pid} = _get_todo_list_pid(state, todo_list_id)
    {
      :reply,
      Todo.List.Server.update_entry(todo_list_pid, entry_id, update_fn),
      state
    }
  end

  @impl GenServer
  def handle_call({:delete_entry, todo_list_id, entry_id}, _caller, state) do
    {state, todo_list_pid} = _get_todo_list_pid(state, todo_list_id)
    {
      :reply,
      Todo.List.Server.delete_entry(todo_list_pid, entry_id),
      state
    }
  end

end
