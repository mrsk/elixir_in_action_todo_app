defmodule Todo.List do
  defstruct(
    next_id: 1,
    entries: %{}
  )

  def new() do
    %Todo.List{}
  end

  def new(entries) do
    entries
    |> Enum.reduce(%Todo.List{}, &add_entry(&2, &1))
  end

  def add_entry(todo_list, entry) do
    entry = Map.put(entry, :id, todo_list.next_id)

    entries =
      Map.put(
        todo_list.entries,
        todo_list.next_id,
        entry
      )

    %Todo.List{
      todo_list
      | entries: entries,
        next_id: todo_list.next_id + 1
    }
  end

  def entries(todo_list, date) do
    todo_list.entries
    |> Map.values()
    |> Enum.filter(&(&1.date == date))
  end

  def update_entry(todo_list, entry_id, update_fn) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list

      {:ok, old_entry} ->
        new_entry = update_fn.(old_entry)
        entries = Map.put(todo_list.entries, new_entry.id, new_entry)
        %Todo.List{todo_list | entries: entries}
    end
  end

  def delete_entry(todo_list, entry_id) do
    entries = Map.delete(todo_list.entries, entry_id)
    %Todo.List{todo_list | entries: entries}
  end
end
