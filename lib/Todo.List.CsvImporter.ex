defmodule Todo.List.CsvImporter do
  def import(path) do
    File.stream!(path)
    |> CSV.Decoding.Parser.parse()
    |> Enum.reduce(%Todo.List{}, fn csv_row_result, todo_list_acc ->
      case csv_row_result do
        {:ok, [date_str, title]} ->
          Todo.List.add_entry(todo_list_acc, %{date: Date.from_iso8601!(date_str), title: title})

        err ->
          IO.inspect(err)
          raise("csv parse error")
      end
    end)
  end
end
