defmodule Todo.List.Server.Test do
  use ExUnit.Case


  test "Todo.List.Server start stop" do
    {:ok, pid} = Todo.List.Server.start()
    pid_info = Process.info(pid)
    assert pid_info != nil
    :ok = Todo.List.Server.stop(pid)
    pid_info = Process.info(pid)
    assert pid_info == nil
  end

  test "Todo.List.Server.add_entry" do
    {:ok, pid} = Todo.List.Server.start()

    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-18], entry2: :entry2})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-18], entry3: :entry3})

    entries = Todo.List.Server.entries(pid, ~D[2024-02-18])
    |> Enum.sort_by(&(&1[:id]))

    assert Enum.count(entries) == 3
    assert entries |> Enum.at(0) == %{id: 1, date: ~D[2024-02-18], entry1: :entry1}
    assert entries |> Enum.at(1) == %{id: 2, date: ~D[2024-02-18], entry2: :entry2}
    assert entries |> Enum.at(2) == %{id: 3, date: ~D[2024-02-18], entry3: :entry3}
  end


  test "Todo.List.Server.entries" do
    {:ok, pid} = Todo.List.Server.start()

    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-19], entry2: :entry2})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-20], entry3: :entry3})

    entries = Todo.List.Server.entries(pid, ~D[2024-02-19])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 2, date: ~D[2024-02-19], entry2: :entry2}
  end

  test "Todo.List.Server.update_entry" do
    {:ok, pid} = Todo.List.Server.start()

    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-19], entry2: :entry2})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-20], entry3: :entry3})

    Todo.List.Server.update_entry(pid, 2, &Map.put(&1, :wat, :wat))

    entries = Todo.List.Server.entries(pid, ~D[2024-02-18])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 1, date: ~D[2024-02-18], entry1: :entry1}

    entries = Todo.List.Server.entries(pid, ~D[2024-02-19])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 2, date: ~D[2024-02-19], entry2: :entry2, wat: :wat}

    entries = Todo.List.Server.entries(pid, ~D[2024-02-20])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 3, date: ~D[2024-02-20], entry3: :entry3}
  end

  test "Todo.List.Server.delete_entry" do
    {:ok, pid} = Todo.List.Server.start()

    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-19], entry2: :entry2})
    Todo.List.Server.add_entry(pid, %{date: ~D[2024-02-20], entry3: :entry3})

    Todo.List.Server.delete_entry(pid, 2)

    entries = Todo.List.Server.entries(pid, ~D[2024-02-18])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 1, date: ~D[2024-02-18], entry1: :entry1}

    entries = Todo.List.Server.entries(pid, ~D[2024-02-19])
    assert Enum.count(entries) == 0

    entries = Todo.List.Server.entries(pid, ~D[2024-02-20])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 3, date: ~D[2024-02-20], entry3: :entry3}
  end

end
