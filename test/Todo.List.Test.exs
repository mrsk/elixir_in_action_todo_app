defmodule Todo.List.Test do
  # alias Todo.List
  use ExUnit.Case

  test "Todo.List.add_entry" do
    todo_list =
      Todo.List.new()
      |> Todo.List.add_entry(%{:entry1 => :entry1})
      |> Todo.List.add_entry(%{:entry2 => :entry2})
      |> Todo.List.add_entry(%{:entry3 => :entry3})

    assert todo_list.next_id == 4
    assert todo_list.entries[1] == %{:id => 1, :entry1 => :entry1}
    assert todo_list.entries[2] == %{:id => 2, :entry2 => :entry2}
    assert todo_list.entries[3] == %{:id => 3, :entry3 => :entry3}
  end

  test "Todo.List.entries" do
    todo_list =
      Todo.List.new()
      |> Todo.List.add_entry(%{date: ~D[2024-02-18], entry1: :entry1})
      |> Todo.List.add_entry(%{date: ~D[2024-02-19], entry2: :entry2})
      |> Todo.List.add_entry(%{date: ~D[2024-02-20], entry3: :entry3})

    filtered =
      todo_list
      |> Todo.List.entries(~D[2024-02-19])

    assert filtered |> Enum.at(0) == %{date: ~D[2024-02-19], entry2: :entry2, id: 2}
  end

  test "Todo.List.update_entry" do
    todo_list =
      Todo.List.new()
      |> Todo.List.add_entry(%{date: ~D[2024-02-18], entry1: :entry1})
      |> Todo.List.add_entry(%{date: ~D[2024-02-19], entry2: :entry2})
      |> Todo.List.add_entry(%{date: ~D[2024-02-20], entry3: :entry3})
      |> Todo.List.update_entry(2, &Map.put(&1, :wat, :wat))

    assert todo_list.entries[2] == %{id: 2, date: ~D[2024-02-19], entry2: :entry2, wat: :wat}
  end

  test "Todo.List.delete_entry" do
    todo_list =
      Todo.List.new()
      |> Todo.List.add_entry(%{date: ~D[2024-02-18], entry1: :entry1})
      |> Todo.List.add_entry(%{date: ~D[2024-02-19], entry2: :entry2})
      |> Todo.List.add_entry(%{date: ~D[2024-02-20], entry3: :entry3})
      |> Todo.List.delete_entry(2)

    assert Map.has_key?(todo_list.entries, 1) == true
    assert Map.has_key?(todo_list.entries, 2) == false
    assert Map.has_key?(todo_list.entries, 3) == true
  end
end
