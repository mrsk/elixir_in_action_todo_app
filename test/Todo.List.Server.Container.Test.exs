defmodule Todo.List.Server.Container.Test do
  # alias Todo.List
  use ExUnit.Case

  test "Todo.List.Server.Container.add_entry" do
    Todo.List.Server.Container.start()
    todo_list_id = :todo_list_0

    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-18], entry2: :entry2})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-18], entry3: :entry3})

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-18])
    |> Enum.sort_by(&(&1[:id]))

    assert Enum.count(entries) == 3
    assert entries |> Enum.at(0) == %{id: 1, date: ~D[2024-02-18], entry1: :entry1}
    assert entries |> Enum.at(1) == %{id: 2, date: ~D[2024-02-18], entry2: :entry2}
    assert entries |> Enum.at(2) == %{id: 3, date: ~D[2024-02-18], entry3: :entry3}

  end

  test "Todo.List.Server.Container.entries" do
    Todo.List.Server.Container.start()
    todo_list_id = :todo_list_1

    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-19], entry2: :entry2})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-20], entry3: :entry3})

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-19])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 2, date: ~D[2024-02-19], entry2: :entry2}
  end

  test "Todo.List.Server.Container.update_entry" do
    Todo.List.Server.Container.start()
    todo_list_id = :todo_list_2

    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-19], entry2: :entry2})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-20], entry3: :entry3})

    Todo.List.Server.Container.update_entry(todo_list_id, 2, &Map.put(&1, :wat, :wat))

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-18])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 1, date: ~D[2024-02-18], entry1: :entry1}

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-19])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 2, date: ~D[2024-02-19], entry2: :entry2, wat: :wat}

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-20])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 3, date: ~D[2024-02-20], entry3: :entry3}
  end


  test "Todo.List.Server.Container.delete_entry" do
    Todo.List.Server.Container.start()
    todo_list_id = :todo_list_3

    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-18], entry1: :entry1})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-19], entry2: :entry2})
    Todo.List.Server.Container.add_entry(todo_list_id, %{date: ~D[2024-02-20], entry3: :entry3})

    Todo.List.Server.Container.delete_entry(todo_list_id, 2)

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-18])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 1, date: ~D[2024-02-18], entry1: :entry1}

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-19])
    assert Enum.count(entries) == 0

    entries = Todo.List.Server.Container.entries(todo_list_id, ~D[2024-02-20])
    assert Enum.count(entries) == 1
    assert entries |> Enum.at(0) == %{id: 3, date: ~D[2024-02-20], entry3: :entry3}
  end
end
