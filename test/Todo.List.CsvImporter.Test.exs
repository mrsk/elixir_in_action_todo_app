defmodule Todo.List.CsvImporter.Test do
  # alias Todo.List
  use ExUnit.Case

  test "Todo.List.CsvImporter.import" do
    todo_list = Todo.List.CsvImporter.import("inputs/todos.csv")

    assert map_size(todo_list.entries) == 3

    assert todo_list.entries[1] == %{id: 1, date: ~D[2023-12-19], title: "Dentist"}
    assert todo_list.entries[2] == %{id: 2, date: ~D[2023-12-20], title: "Shopping"}
    assert todo_list.entries[3] == %{id: 3, date: ~D[2023-12-19], title: "Movies"}
  end
end
